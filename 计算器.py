"""
    作者：王泽文
    文件名：计算器
    时间：2020年4月12日
"""
import math


# 表达式运算
def test(date):
    result = eval(date)
    return result


# 加法
def add(a, b):
    return a + b


# 减法
def minus(a, b):
    return a - b


# 乘法
def mul(a, b):
    return a * b


# 除法
def div(a, b):
    if b == 0:
        print("Error")
    else:
        return a / b


# 求模
def mod(a, b):
    return a % b


# 幂运算
def power(a, b):
    return a ** b


while True:
    print("计算器")
    i = int(input("基本运算输1,表达式运算输2,高级功能输3,输0退出\n请输入："))
    if i == 1:
        print("1：加法 2：减法 3：乘法 4：除法 5：求模 6：幂运算\n")
        s = int(input("请输入运算方法："))
        n1 = float(input("请输入第一个数："))
        n2 = float(input("请输入第二个数："))
        print("结果为：")
        if s == 1:
            print(add(n1, n2))
        if s == 2:
            print(minus(n1, n2))
        if s == 3:
            print(mul(n1, n2))
        if s == 4:
            print(div(n1, n2))
        if s == 5:
            print(mod(n1, n2))
        if s == 6:
            print(power(n1, n2))
    elif i == 2:
        print("支持加减乘除和求模(%)与幂函数(**)\n示例：3+4-(3-2)**2")
        m = input("请输入表达式:")
        print("结果为：", test(m))
    elif i == 3:
        print("1：对数函数 2：开方 3：向下取整 4：向上取整 5：弧度转换角度 6：角度转换弧度 7：正弦函数 8：余弦函数 9：正切函数 10：最大公约数")
        s = int(input("请输入运算方法："))
        if s == 1:
            print("对数运算")
            n3 = float(input("请输入运算数："))
            n4 = float(input("请输入底数："))
            print("结果为：", math.log(n3, n4))
        elif s == 10:
            print("最大公约数")
            n5 = float(input("请输入第一个运算数："))
            n6 = float(input("请输入第二个运算数："))
            print("结果为：", math.gcd(n5, n6))
        elif 2 <= s <= 9:
            n7 = float(input("请输入运算数（三角函数仅输入弧度制pi前的系数,范围0到1）："))
            if s == 2:
                print("开方运算\n结果为：", math.sqrt(n7))
            if s == 3:
                print("向下取整\n结果为：", math.trunc(n7))
            if s == 4:
                print("向上取整\n结果为：", math.ceil(n7))
            if s == 5:
                print("弧度转换角度\n结果为：", math.degrees(n7))
            if s == 6:
                print("角度转换弧度\n结果为：", math.radians(n7))
            if s == 7:
                print("正弦函数\n结果为：", math.sin(math.pi*n7))
            if s == 8:
                print("余弦函数\n结果为：", math.cos(math.pi*n7))
            if s == 9:
                print("正切函数\n结果为：", math.tan(math.pi*n7))
    elif i == 0:
        break