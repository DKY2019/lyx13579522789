'''
作者：刘宇轩
文件名：列表实践
时间：2021.3.29
'''
#二战德国空军科技树
战斗机 = ["he-51","he112","bf109","bf109G","ta152","fw190"]
截击机 = ["bf110","me410","do217","hs129"]
前线轰炸机 = ["do17","he-111","ju88","sm.79"]
战略轰炸机 = ["fw200c","me264","ju288c"]
喷气机 = ["ho229","me163","me262"]
while True:
    print("二战德国空军科技树\n查询:1/增加:2/修改:3/删除:4/退出:5")
    a = int(input("请输入："))
    if a == 1:
        print ("战斗机",战斗机,
                "截击机",截击机,
                "前线轰炸机",前线轰炸机,
                "战略轰炸机",战略轰炸机,
                "喷气机",喷气机,
                sep='\n'
                )
        continue
    if a == 2 or a == 3 or a == 4:
        print ("战机类型：\n战斗机/截击机/前线轰炸机/战略轰炸机/喷气机")
        b = input("请输入战机类型：")
        c = input("请输入机型：")
        if b == "战斗机":
            print (战斗机)
            if a == 2:
                if c in 战斗机:
                    print ("该机型已存在")
                else:
                    战斗机.append(c)
                    print ("添加成功\n",战斗机)
            if a == 3:
                if c in 战斗机:
                    d = 战斗机.index(c)
                    战斗机[d] = input("替换机型名为：")
                    print("修改成功\n",战斗机)
                else:
                    print("该机型不存在")
            if a == 4:
                if c in 战斗机:
                    战斗机.remove(c)
                    print("删除成功\n",战斗机)
                else:
                    print("该机型不存在")
            continue
    if a == 5:
        print ("谢谢使用，再会！")
        break
'''
由于时间问题只写了  战斗机list  的各项操作
其他 list 后续 添加
'''