'''
作者：刘宇轩
文件名称：仿射加密
时间：2021.3.23
'''
'''
本程序暂时仅支持26个英文字母输入（支持大小写）
'''
def encrypt():  # 加密
    x = input()
    en1 = []
    de1 = []
    for i in range(len(x)):
        if ord(x[i]) >= 97:
            en1.append(chr(((ord(x[i]) - 97) * 5 + 8) % 26 + 65))
        if 97 > ord(x[i]) >= 65:
            en1.append(chr(((ord(x[i]) - 65) * 5 + 8) % 26 + 97))
    en2 = ''.join(en1)
    for i in range(len(en2)):
        if ord(en2[i]) >= 97:
            de1.append(chr((((ord(en2[i]) - 97) - 8) * 21) % 26 + 65))
        if 97 > ord(en2[i]) >= 65:
            de1.append(chr((((ord(en2[i]) - 65) - 8) * 21) % 26 + 97))
    de2 = ''.join(de1)
    print("密文： " + en2 + "加密验证：" + de2)


def decode1():  # 解密
    y = input()
    de = []
    for i in range(len(y)):
        if ord(y[i]) >= 97:
            de.append(chr((((ord(y[i]) - 97) - 8) * 21) % 26 + 65))
        if 97 > ord(y[i]) >= 65:
            de.append(chr((((ord(y[i]) - 65) - 8) * 21) % 26 + 97))
    de3 = ''.join(de)
    print("明文： " + de3)

while (1):
    a=int(input("1.加密  2.解密  3.结束程序\n"))
    if a==1:
        print("请输入要加密的明文")
        encrypt()

    if a==2:
        print("请输入要解密的密文")
        decode1()
    if a==3:
        print("谢谢使用！再会")
        break

