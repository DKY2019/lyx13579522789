c = "人生苦短，我用python"
# 定义字符串变量
print(c)
a = "12"
b = int(a)
# 将字符串变量转化为整数类型
print(a)
print(b)
# 这是单行注释
"""
这是
多行注释
"""
# 调试
a = 0
while a < 10:
   a += 1
   print(a)