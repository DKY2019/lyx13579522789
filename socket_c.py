
'''
作者：刘宇轩
文件名称：socket_c
时间：2021.5.30
'''
import socket
import base64


def base64_encode(ad, name):
    f0 = open(ad + name, "r+")
    str = f0.read()
    pp = str.encode('utf-8')
    pp = base64.b64encode(pp)
    pp = pp.decode('utf-8')
    return pp


while True:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 60000))
    s.sendall("连接成功".encode('utf-8'))
    a = int(input("1.通信 2.传输文件 3.退出\n请输入："))
    if a == 1:
        str = input("输入：")
        s.sendall(str.encode('utf-8'))
        data = s.recv(1024)
        print(data.decode('utf-8'))
    elif a == 2:
        s.sendall("正在发送文件".encode('utf-8'))
        ad = input("请输入文件地址:")
        name = input("请输入文件名:")
        s.sendall(name.encode('utf-8'))
        str = base64_encode(ad, name)
        s.sendall(str.encode('utf-8'))
        data = s.recv(1024)
        print(data.decode('utf-8'))

    else:
        str = "对方已退出"
        s.sendall(str.encode('utf-8'))
        print("已退出")
        s.close()
        break