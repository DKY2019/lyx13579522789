'''
作者：刘宇轩
文件名称：python综合实践
时间：2021.6.24
'''



import pygame
import sys
import random
import time
class Bird(object):
    def __init__(self):
        b = random.randint(0,2)
        if b == 1 :
            self.status = [pygame.image.load('./final/bird0_0.png'),pygame.image.load('./final/bird0_1.png'),
                           pygame.image.load('./final/bird0_2.png'),pygame.image.load('./final/medals_2.png')]
        if b== 0:
            self.status = [pygame.image.load('./final/bird1_0.png'), pygame.image.load('./final/bird1_1.png'),
                           pygame.image.load('./final/bird1_2.png'),pygame.image.load('./final/medals_2.png')]
        if b == 2 :
            self.status = [pygame.image.load('./final/bird2_0.png'), pygame.image.load('./final/bird2_1.png'),
                           pygame.image.load('./final/bird2_2.png'),pygame.image.load('./final/medals_2.png')]
        #self.xixi=pygame.image.load('./final/bird2_0.png')
        self.birdrect=pygame.Rect(144,256,48,48)
        self.body = 0
        self.birdx=144
        self.birdy=256
        self.jump=False
        self.jumpspeed=10
        self.gravity=5
        self.dead=False
    def birdUpdate(self):
        if self.jump :
            #self.gravity = 5
            self.jumpspeed -=1
            self.birdy =self.birdy-self.jumpspeed
            #self.jump = False
        else:
            #self.gravity=5
            self.gravity+=0.2
            self.birdy = self.birdy+self.gravity
        self.birdrect[1]=self.birdy
class Line(object):
    def __init__(self):
        #st_tm = time.time()
        self.wallx=288
        self.wally1=random.randint(-230,-120)
        self.wally2=random.randint(300,500)
        print(self.wally2)
        self.wallup = pygame.image.load('./final/pipe2_up.png')
        self.walldown=pygame.image.load('./final/pipe_down.png')
    def lineUpdate(self):
        #st_tm = time.time()
        self.wallx =self.wallx-5
        if self.wallx < -52:
            global score
            score=score+1
            wally1 = random.randint(-230,-120)
            wally2 =random.randint(300,500)
            self.wallx =288
def start():
    pygame.init()
    size = width, height = 288, 512
    screen = pygame.display.set_mode((288, 512))
    #clock = pygame.time.Clock()
    while True:
        w =0
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN:
                w=1
                break
        if w==1 :
            break
        start=pygame.image.load('./final/button_play.png')
        screen.fill((255, 255, 255))
        screen.blit(start, (100,230))
        pygame.display.flip()
    #pygame.quit()
#def checkdead():
 #   uprect=pygame.Rect(Line.wallx,Line.wally1,Line.wallup.get_width(),Line.wallup.get_height())
  #  downrect = pygame.Rect(Line.wallx, Line.wally2, Line.walldown.get_width(),Line.walldown.get_height())
   # if uprect.colliderect(bird.birdrect) or downrect.colliderect(bird.birdrect) :
    #    bird.dead = True
    #return  bird.dead
#def getresult():
if __name__ == '__main__':
    pygame.init()
    size = width, height = 288, 512
    screen = pygame.display.set_mode((288, 512))
    clock = pygame.time.Clock()
    #color=(255,255,255)
    a = random.randint(0, 1)
    #print(a)
    pygame.font.init()
    font = pygame.font.SysFont(None,70)
    bird =Bird()
    Line = Line()
    score=0
    start()
    while True:
        clock.tick(50)
        #bird = Bird()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if (event.type == pygame.KEYDOWN or event.type == pygame.MOUSEBUTTONDOWN) and not bird.dead:
                #screen.blit(bird.status[1], (bird.birdx, bird.birdy))
                bird.jump = True
                bird.jumpspeed = 10
                bird.gravity = 5
                #bird.birdUpdate()
                #bird.jump =False
                #screen.blit(bird.status[2], (bird.birdx, bird.birdy))
            #else:
             #   bird.jump=False
              #  bird.jumpspeed += 1
               # bird.gravity = 5
        if a == 1 :
            background = pygame.image.load('./final/bg_day.png')
        if a== 0:
            background = pygame.image.load('./final/bg_night.png')
        screen.blit(background, (0,0))
        #if Line.wallx<bird.birdx<Line.wallx+30:
        if Line.wallx == -52:
            Line.wally1=random.randint(-230,-100)
            Line.wally2=random.randint(250,320)
        #uprect = pygame.Rect(Line.wallx, Line.wally1, Line.wallup.get_width(), Line.wallup.get_height()-30)
        #downrect = pygame.Rect(Line.wallx, Line.wally2, Line.walldown.get_width(), Line.walldown.get_height()-Line.wally2)
        uprect = pygame.Rect(Line.wallx, Line.wally1, Line.wallup.get_width(), Line.wallup.get_height())
        downrect = pygame.Rect(Line.wallx, Line.wally2, Line.walldown.get_width(), Line.walldown.get_height())
        #print(Line.wallup.get_height()+Line.wally1,Line.walldown.get_height()-Line.wally2)
        #print(Line.wallup.get_width(),Line.wallup.get_height())
        if uprect.colliderect(bird.birdrect) or downrect.colliderect(bird.birdrect) or bird.birdy>450 or bird.birdy<0:
            #screen.blit(bird.status[1], (bird.birdx, bird.birdy))
            bird.dead = True
            bird.body = 3
            screen.blit(bird.status[3], (bird.birdx, bird.birdy))
            pygame.display.flip()
        #if checkdead():
        if bird.dead == True:
            time.sleep(1)
            pygame.init()
            size = width, height = 288, 512
            screen = pygame.display.set_mode((288, 512))
            #clock = pygame.time.Clock()
            screen.fill((0, 0, 0))
            gameover=pygame.image.load('./final/text_game_over.png')
            screen.blit(gameover, (30, 230))
            pygame.display.flip()
            break
            #pygame.quit()
        else:
        #Line.lineUpdate()
            #if Line.wallx == -52:
             #   Line.wally1=random.randint(-230,-100)
             #   Line.wally2=random.randint(250,320)
            screen.blit(Line.wallup,(Line.wallx,Line.wally2))
            screen.blit(Line.walldown, (Line.wallx, Line.wally1))
            Line.lineUpdate()
        #screen.fill(color)
            #if bird.dead :
             #   bird.body = 3
            #el
            if bird.jump == True:
                #bird.body =1
                screen.blit(bird.status[1], (bird.birdx, bird.birdy))
            else:
                screen.blit(bird.status[2], (bird.birdx, bird.birdy))
            #bird.body = 0
            #screen.blit(bird.status[bird.body],(bird.birdx,bird.birdy))
            bird.birdUpdate()
            #bird.jump =False
            #bird.jumpspeed = 10
            #bird.gravity = 5
            title=pygame.image.load('./final/title.png')
            land=pygame.image.load('./final/land.png')
            screen.blit(land, (0, 450))
            screen.blit(title, (0, 0))
            screen.blit(font.render(":"+str(score),-1,(255,255,255)), (179, 0))
            pygame.display.flip()
        #pygame.display.update()
    pygame.quit()